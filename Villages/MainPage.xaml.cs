﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using Villages.Resources;
using Microsoft.Devices.Sensors;
using Microsoft.Xna.Framework;
using Windows.Networking.Proximity;
using Windows.Networking.Sockets;
using System.Windows.Media;
using System.Threading;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Windows.Threading;
using Microsoft.Devices;
using System.Windows.Media.Imaging;
using System.Threading.Tasks;
using System.Diagnostics;
using Windows.Storage.Streams;
using System.Text;

namespace Villages
{
    public partial class MainPage : PhoneApplicationPage
    {
        Accelerometer accelerometer;
        StreamSocket _socket;
        string _receiveBuffer = "";
        DateTime _pingSentTime;
        // Constructor
        public MainPage()
        {
            InitializeComponent();
            if (!Accelerometer.IsSupported)
            {
                MessageBox.Show("Este dispositivo no cuenta con acelerómetro");
                ActionButton.IsEnabled = false;
                StopButton.IsEnabled = false;
            }
            else {
                TryConnect();
            }
            // Sample code to localize the ApplicationBar
            //BuildLocalizedApplicationBar();
        }


        private void ActionButton_Click(object sender, RoutedEventArgs e)
        {
                startAccelometer();
                Write('1');
      
        }


        private void startAccelometer() {
            if (accelerometer == null)
            {
                accelerometer = new Accelerometer();
                accelerometer.TimeBetweenUpdates = TimeSpan.FromMilliseconds(20);
                accelerometer.CurrentValueChanged +=
                    new EventHandler<SensorReadingEventArgs<AccelerometerReading>>(accelerometer_CurrentValueChanged);
            }
            try
            {
                //MessageBox.Show("Iniciando Aplicación");
                accelerometer.Start();
            }
            catch (InvalidOperationException ex)
            {
                MessageBox.Show("No se pudo iniciar la aplicación.");
            }
        }

        private async void TryConnect()
        {
            //connectingOverlay.Visibility = System.Windows.Visibility.Visible;
            PeerFinder.AlternateIdentities["Bluetooth:Paired"] = "";
            var pairedDevices = await PeerFinder.FindAllPeersAsync();
            if (pairedDevices.Count == 0)
            {
                MessageBox.Show("No hay dispositivos emparejados");
            }
            else
            {
                try
                {
                    PeerInformation selectedDevice = pairedDevices[0];
                    _socket = new StreamSocket();
                    await _socket.ConnectAsync(selectedDevice.HostName, "1");
                    WaitForData(_socket);
                    MessageBox.Show("El carrito esta conectado");
                    //Write('C');
                }
                catch
                {
                    MessageBox.Show("Carrito desconectado");
                }
            }
        }

        async private void WaitForData(StreamSocket socket)
        {
            try
            {
                byte[] bytes = new byte[5];
                await socket.InputStream.ReadAsync(bytes.AsBuffer(), 5, InputStreamOptions.Partial);
                bytes = bytes.TakeWhile((v, index) => bytes.Skip(index).Any(w => w != 0x00)).ToArray();
                string str = Encoding.UTF8.GetString(bytes, 0, bytes.Length);
                if (str.Contains("|"))
                {
                    _receiveBuffer += str.Substring(0, str.IndexOf("|"));
                    DoSomethingWithReceivedString(_receiveBuffer);
                    _receiveBuffer = str.Substring(str.IndexOf("|") + 1);
                }
                else
                {
                    _receiveBuffer += str;
                }

            }
            catch
            {
                TryConnect();
            }
            finally
            {
                WaitForData(socket);
            }
        }

        private void DoSomethingWithReceivedString(string buffer)
        {
            if (buffer == "ping")
            {
                MessageBox.Show("ola k ase, no enviando ping o k ase" + DateTime.Now.Subtract(_pingSentTime).TotalMilliseconds + "MS");
            }
            else
            {
                MessageBox.Show(buffer);
            }

        }

        async private void Write(char str)
        {
            try
            {
                var dataBuffer = GetBufferFromByteArray(Encoding.UTF8.GetBytes(str + ""));
                await _socket.OutputStream.WriteAsync(dataBuffer);
            }
            catch
            {
                MessageBox.Show("Carrito desconectado");
                TryConnect();
            }
        }

        private IBuffer GetBufferFromByteArray(byte[] package)
        {
            using (DataWriter dw = new DataWriter())
            {
                dw.WriteBytes(package);
                return dw.DetachBuffer();
            }
        }


        void accelerometer_CurrentValueChanged(object sender, SensorReadingEventArgs<AccelerometerReading> e)
        {
            // Call UpdateUI on the UI thread and pass the AccelerometerReading.
            Dispatcher.BeginInvoke(() => UpdateUI(e.SensorReading));
        }

        private void UpdateUI(AccelerometerReading accelerometerReading)
        {

            Vector3 acceleration = accelerometerReading.Acceleration;

            if (acceleration.Y > 0.6) {
                    Write('2');
       
                //Thread.Sleep(100);
            }
            if (acceleration.Y < -0.6)
            {
                    Write('3');
        
               //Thread.Sleep(100);
            }
             
            /*if (acceleration.Y < 0.59 && acceleration.Y > -0.61){
                Write('6');
            }

            /*
            if (acceleration.Z < -0.6)
            {
                Write('1');
            }
            
            if (acceleration.Z > 0.6)
            {
                Write('4');
            }

            if (acceleration.Z < 0.59 && acceleration.Z > -0.61)
            {
          
                Write('5');
            }*/

        }

        private void StopButton_Click(object sender, RoutedEventArgs e)
        {
            if (accelerometer != null)
            {
                // Stop the accelerometer.
                accelerometer.Stop();
                //MessageBox.Show("Acelerómetro detenido");
                Write('5');
            }
        }

        private void ReverseButton_Click(object sender, RoutedEventArgs e)
        {
                startAccelometer();
                Write('4');
        
        }

        // Sample code for building a localized ApplicationBar
        //private void BuildLocalizedApplicationBar()
        //{
        //    // Set the page's ApplicationBar to a new instance of ApplicationBar.
        //    ApplicationBar = new ApplicationBar();

        //    // Create a new button and set the text value to the localized string from AppResources.
        //    ApplicationBarIconButton appBarButton = new ApplicationBarIconButton(new Uri("/Assets/AppBar/appbar.add.rest.png", UriKind.Relative));
        //    appBarButton.Text = AppResources.AppBarButtonText;
        //    ApplicationBar.Buttons.Add(appBarButton);

        //    // Create a new menu item with the localized string from AppResources.
        //    ApplicationBarMenuItem appBarMenuItem = new ApplicationBarMenuItem(AppResources.AppBarMenuItemText);
        //    ApplicationBar.MenuItems.Add(appBarMenuItem);
        //}
    }
}
