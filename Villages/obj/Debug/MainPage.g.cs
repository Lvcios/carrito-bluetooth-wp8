﻿#pragma checksum "C:\Users\juan\Documents\Visual Studio 2012\Projects\Villages\Villages\MainPage.xaml" "{406ea660-64cf-4c82-b6f0-42d48172a799}" "2FDE3DC614F1A7C3804AFBAD50459EE4"
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.18051
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using Microsoft.Phone.Controls;
using System;
using System.Windows;
using System.Windows.Automation;
using System.Windows.Automation.Peers;
using System.Windows.Automation.Provider;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Interop;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Imaging;
using System.Windows.Resources;
using System.Windows.Shapes;
using System.Windows.Threading;


namespace Villages {
    
    
    public partial class MainPage : Microsoft.Phone.Controls.PhoneApplicationPage {
        
        internal System.Windows.Controls.Grid LayoutRoot;
        
        internal System.Windows.Controls.StackPanel TitlePanel;
        
        internal System.Windows.Controls.Grid ContentPanel;
        
        internal System.Windows.Controls.Button ActionButton;
        
        internal System.Windows.Controls.Button StopButton;
        
        internal System.Windows.Controls.TextBlock yTextBlock;
        
        internal System.Windows.Shapes.Line yLine;
        
        internal System.Windows.Shapes.Line zLine;
        
        internal System.Windows.Controls.Button ReverseButton;
        
        private bool _contentLoaded;
        
        /// <summary>
        /// InitializeComponent
        /// </summary>
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        public void InitializeComponent() {
            if (_contentLoaded) {
                return;
            }
            _contentLoaded = true;
            System.Windows.Application.LoadComponent(this, new System.Uri("/Villages;component/MainPage.xaml", System.UriKind.Relative));
            this.LayoutRoot = ((System.Windows.Controls.Grid)(this.FindName("LayoutRoot")));
            this.TitlePanel = ((System.Windows.Controls.StackPanel)(this.FindName("TitlePanel")));
            this.ContentPanel = ((System.Windows.Controls.Grid)(this.FindName("ContentPanel")));
            this.ActionButton = ((System.Windows.Controls.Button)(this.FindName("ActionButton")));
            this.StopButton = ((System.Windows.Controls.Button)(this.FindName("StopButton")));
            this.yTextBlock = ((System.Windows.Controls.TextBlock)(this.FindName("yTextBlock")));
            this.yLine = ((System.Windows.Shapes.Line)(this.FindName("yLine")));
            this.zLine = ((System.Windows.Shapes.Line)(this.FindName("zLine")));
            this.ReverseButton = ((System.Windows.Controls.Button)(this.FindName("ReverseButton")));
        }
    }
}

